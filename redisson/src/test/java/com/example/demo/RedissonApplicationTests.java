package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RLock;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/4/16 16:27
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class RedissonApplicationTests {
    @Autowired
    private RedissonClient redisson;


    private static int clientTotal = 20000;
    private static int threadTotal = 200;

    private static int count = 0;
    private static AtomicInteger successCount = new AtomicInteger(0);
    private static AtomicInteger failCount = new AtomicInteger(0);
    private static AtomicInteger errorCount = new AtomicInteger(0);


    @Test
    public void test1() throws InterruptedException {
        ExecutorService executorService = Executors.newCachedThreadPool();
        final Semaphore semaphore = new Semaphore(threadTotal);
        final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        for (int i = 0; i < clientTotal; i++) {
            int finalI = i;
            executorService.execute(() -> {
                try {
                    semaphore.acquire();
                    lockTest("" + finalI);
                    semaphore.release();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
                countDownLatch.countDown();
            });

        }
        countDownLatch.await();
        executorService.shutdown();
        log.info("successCount: {}", successCount);
        log.info("failCount: {}", failCount);
        log.info("errorCount: {}", errorCount);
    }
    //可用
    private void lockTest(String threadId) {
        RLock lock = redisson.getLock("testLock");
        try {
            if (!lock.tryLock()) {
//                log.error("重复操作: {}", threadId);
                failCount.incrementAndGet();
                return;
            }
            Thread.sleep(10);
            successCount.incrementAndGet();
        } catch (Throwable throwable) {
            log.error("异常", throwable);
        } finally {
            try {
                lock.forceUnlock();
            } catch (Exception e) {
                errorCount.incrementAndGet();
            }
        }
    }

    private void lockTest2(String threadId) {
        RLock lock = redisson.getLock("testLock2");
        try {
            lock.lock();
            count++;
        } catch (Throwable throwable) {
            log.error("异常", throwable);
        } finally {
            lock.forceUnlock();
        }
    }


}

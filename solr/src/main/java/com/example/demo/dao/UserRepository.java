package com.example.demo.dao;

import com.example.demo.model.User;
import org.springframework.data.solr.repository.SolrCrudRepository;

import java.util.List;

/**
 * 用户数据访问
 *
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/1/14 21:16
 */
public interface UserRepository extends SolrCrudRepository<User, String> {
    List<User> findByName(String name);
}

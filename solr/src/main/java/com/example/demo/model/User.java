package com.example.demo.model;

import lombok.Data;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

/**
 * 用户信息
 *
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/1/14 21:14
 */
@SolrDocument(solrCoreName = "test")
@Data
public class User {
    @Id
    private String id;
    @Field
    private String name;
    @Field
    private String photo;
}

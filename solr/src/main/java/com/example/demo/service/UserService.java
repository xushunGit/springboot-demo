package com.example.demo.service;

import com.example.demo.model.User;

import java.util.List;

/**
 * 用户服务接口
 *
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/1/14 21:22
 */
public interface UserService {
    User findById(String id);
    User save(User user);
    List<User> findAll();
    List<User> findByName(String name);
    void deleteById(String id);
}

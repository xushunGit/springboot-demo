package com.example.demo.controller;

import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户控制器
 *
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/1/14 21:34
 */
@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("findById/{id}")
    public User findById(@PathVariable String id) {
        return userService.findById(id);
    }

    @GetMapping("save")
    public User save(@RequestBody User user) {
        return userService.save(user);
    }

    @GetMapping("findAll")
    public List<User> findAll() {
        return userService.findAll();
    }

    @GetMapping("findByName/{name}")
    public List<User> findByName(@PathVariable String name) {
        return userService.findByName(name);
    }

    @GetMapping("deleteById/{id}")
    public String deleteById(@PathVariable String id){
        userService.deleteById(id);
        return "success";
    }

}

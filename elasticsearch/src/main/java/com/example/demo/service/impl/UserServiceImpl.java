package com.example.demo.service.impl;

import com.example.demo.dao.UserRepository;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/4/9 18:51
 */
@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public User findById(String id) {
        Optional<User> optional = userRepository.findById(id);
        return optional.isPresent() ? optional.get() : null;
    }

    @Override
    public User save(User user){
        return userRepository.save(user);
    }

    @Override
    public List<User> findAll() {
        Iterable<User> all = userRepository.findAll();
        List<User> userList = new ArrayList<>();
        all.forEach(user -> userList.add(user));
        return userList;
    }

    @Override
    public List<User> findByName(String name) {
        return userRepository.findByName(name);
    }

    @Override
    public void deleteById(String id) {
        userRepository.deleteById(id);
    }
}

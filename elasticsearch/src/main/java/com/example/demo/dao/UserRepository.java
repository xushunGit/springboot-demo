package com.example.demo.dao;

import com.example.demo.model.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import java.util.List;

/**
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/4/9 18:51
 */
public interface UserRepository extends ElasticsearchCrudRepository<User, String> {
    List<User> findByName(String name);
}

package com.example.demo.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * 用户实体
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/1/14 16:52
 */
@Data
public class User {
    @Id
    private String id;
    private String name;
    private String password;
}

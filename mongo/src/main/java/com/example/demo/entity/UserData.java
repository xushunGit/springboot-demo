package com.example.demo.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 用户数据
 *
 * @author xushun
 * @date 2019/1/19 13:35
 */
@Data
@Document("UserData")
public class UserData {
    @Id
    private String _id;
    private String userId;// 用户ID

    private Date lastLoginTime; // 最后登陆时间
    private Date lastLogoutTime;// 最后退出时间
    private long onlineTime; // 总累计在线时间(s秒)
    private long dayOnlinePoint;// 当前获得的在线积分
    private long dayOnlineTime;// 当天累计在线时间(s秒)
    private Date lastResetDailyTime;// 上次重置每日数据时间
    private int onlineDay;// 累计在线天数

    private boolean addSysGMember;// 是否已经加入腾讯IM系统群组
    private Map<String, Integer> dailyTask;// 每日任务taskId-reachNum(-1代表已领取)
    private Map<String, Integer> dailyTaskDays;// 每日任务taskId-days(累计领取天数)
    private Map<String, Integer> achievementTask;// 成就任务taskId-reachNum(-1代表已领取)
    private Map<String, Integer> commonTask;// 常规计数任务

    private boolean roomFollowType;// 是否房间跟随模式见ERoomFollowType
    private boolean openTips;// 是否开播提示
    private boolean shakeMicUp;// 摇一摇上mic

    private Map<String, Date> lookMes;// 谁看过我
    private Date lastLookMeTime; // 最后查看谁看过到的时间
    // 临时缓存
    private List<String> lookMeList = new Vector<String>();

    private String useCarId;// 使用中的坐驾ID
    private Map<String, String> friendRemark; // 好友备注
    private AtomicInteger newNotes;// 新互动消息数量(0没有新信息，>0有新消息)
    private boolean setPassword;// 是否已经手动设置密码(只对第三方登陆有效)
    private int errPwdCount;// 修改密码错误次数
    private long firstReChargeVipExpire;// 首冲VIP，buff到期时间
    private int pointRate;// 经验倍率，默认10（需要除以10）
    private Map<String, Integer> luckyDrawTime;// 抽奖次数
    private String refreshMaxMomentId;// 刷新时最大动态id
    private List<Object> squareMomentIds;// 用户动态广场列表
}

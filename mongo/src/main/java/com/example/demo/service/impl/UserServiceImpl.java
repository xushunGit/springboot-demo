package com.example.demo.service.impl;

import com.alibaba.fastjson.JSON;
import com.example.demo.dao.UserRepository;
import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户服务实现类
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/1/14 16:52
 */
@Service("UserService")
@Slf4j
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    public User addUser(User user) {
        User save = userRepository.save(user);
        log.info("save user: {}", JSON.toJSONString(save));
        return save;
    }

    public User findByName(String name) {
        return userRepository.findByName(name);
    }

    public User updatePwdByName(String name, String password) {
        User user = userRepository.findByName(name);
        user.setPassword(password);

        return userRepository.save(user);
    }

    public void deleteById(String id) {
        userRepository.deleteById(id);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

}

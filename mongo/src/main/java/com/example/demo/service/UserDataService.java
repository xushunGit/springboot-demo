package com.example.demo.service;

import com.example.demo.entity.UserData;

import java.util.List;

/**
 * 用户数据
 *
 * @author xushun
 * @date 2019/1/19 13:37
 */
public interface UserDataService {
    List<UserData> findAll();

    void uptableSquareMomentIds();
}

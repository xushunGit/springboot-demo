package com.example.demo.service.impl;

import com.example.demo.dao.UserDataRepository;
import com.example.demo.entity.UserData;
import com.example.demo.service.UserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户数据
 *
 * @author xushun
 * @date 2019/1/19 13:38
 */
@Service
public class UserDataServiceImpl implements UserDataService {

    @Autowired
    private UserDataRepository userDataRepository;

    @Override
    public List<UserData> findAll() {
        return userDataRepository.findAll();
    }

    @Override
    public void uptableSquareMomentIds() {
        List<UserData> all = userDataRepository.findAll();
        List<UserData> list = new ArrayList<>();
        for (UserData userData : all) {
            List<Object> squareMomentIds = userData.getSquareMomentIds();
            List<Object> squareMomentIdList = new ArrayList<>();
            if (squareMomentIds != null) {
                for (Object obj : squareMomentIds) {
                    if(obj instanceof List){
                        List<String> listStr = (List<String>) obj;
                        squareMomentIdList.addAll(listStr);
                    }
                }
            }
            userData.setSquareMomentIds(squareMomentIdList);
            list.add(userData);
        }
        userDataRepository.saveAll(list);
    }
}

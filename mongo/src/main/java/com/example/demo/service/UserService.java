package com.example.demo.service;

import com.example.demo.entity.User;

import java.util.List;

/**
 * 用户服务接口
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/1/14 16:52
 */
public interface UserService {

    User addUser(User user);

    User findByName(String name);

    User updatePwdByName(String name, String password);

    void deleteById(String id);

    List<User> findAll();


}

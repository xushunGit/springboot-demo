package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 用户Controller
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/1/14 16:52
 */
@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("addUser/{name}/{password}")
    public User addUser(@PathVariable String name, @PathVariable String password){
        User user = new User();
        user.setName(name);
        user.setPassword(password);
        return userService.addUser(user);
    }

    @GetMapping("findByName/{name}")
    public User findByName(@PathVariable String name){
        return userService.findByName(name);
    }

    @GetMapping("updatePwdByName/{name}/{password}")
    public User updatePwdByName(@PathVariable String name, @PathVariable String password){
        return userService.updatePwdByName(name, password);
    }

    @GetMapping("deleteById/{id}")
    public String deleteById(@PathVariable String id){
        userService.deleteById(id);
        return "del success!";
    }

    @GetMapping("findAll")
    public List<User> findAll(){
        return userService.findAll();
    }

    @GetMapping("findAll2")
    public List<User> findAll2(){
        return userService.findAll();
    }
}

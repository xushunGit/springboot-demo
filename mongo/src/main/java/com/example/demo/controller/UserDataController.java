package com.example.demo.controller;

import com.example.demo.entity.UserData;
import com.example.demo.service.UserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 用户数据
 *
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/1/19 13:41
 */
@RestController
@RequestMapping("userData")
public class UserDataController {
    @Autowired
    private UserDataService userDataService;

    @GetMapping("findAll")
    public List<UserData> findAll(){
        return userDataService.findAll();
    }
    @GetMapping("uptableSquareMomentIds")
    public String uptableSquareMomentIds(){
        userDataService.uptableSquareMomentIds();
        return "success";
    }
}

package com.example.demo.dao;

import com.example.demo.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * 用户数据接口
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/1/14 16:52
 */
@Repository
public interface UserRepository extends MongoRepository<User, String> {
    User findByName(String name);
}

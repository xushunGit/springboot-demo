package com.example.demo.dao;

import com.example.demo.entity.UserData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * 用户数据查询
 *
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/1/19 13:36
 */
@Repository
public interface UserDataRepository extends MongoRepository<UserData, String> {
}

package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/4/20 16:13
 */
@Service
public class MailService {
    @Autowired
    private JavaMailSender mailSender; //框架自带的

    @Async  //意思是异步调用这个方法
    public void sendMail(String title, String url, String email) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("xushun_mail@163.com"); // 发送人的邮箱
        message.setSubject(title); //标题
        message.setTo(email); //发给谁  对方邮箱
        message.setText(url); //内容
        mailSender.send(message); //发送
    }

    public void sendMailHtml(String title, String html, String email) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
        helper.setFrom("xushun_mail@163.com"); // 发送人的邮箱
        helper.setSubject(title); //标题
        helper.setTo(email);
        helper.setText(html, true); //内容
        mailSender.send(message); //发送
    }
}

package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import java.util.Properties;

/**
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/4/20 16:48
 */
@Component
public class MailConfig {
    @Bean
    public JavaMailSender JavaMailSender(){
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.163.com");
        mailSender.setUsername("xushun_mail@163.com");
        mailSender.setPassword("123456");
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.starttls.enable", "true");
        properties.setProperty("mail.smtp.starttls.required", "true");
        mailSender.setJavaMailProperties(properties);
        return mailSender;
    }
}

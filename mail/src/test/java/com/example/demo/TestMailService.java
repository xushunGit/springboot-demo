package com.example.demo;

import com.example.demo.service.MailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/4/20 16:16
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestMailService {
    @Autowired
    private MailService mailService;

    @Test
    public void test1(){
        mailService.sendMail("测试邮件","这是一个测试邮件", "505232797@qq.com");
    }
}

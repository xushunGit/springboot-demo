package com.example.demo.consumer;

import com.alibaba.fastjson.JSON;
import com.example.demo.model.Order;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * 延迟消息接收
 *
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/4/9 11:25
 */
@Component
@Slf4j
public class LazyMsgReceiver {
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "message.center.create" ),
            exchange = @Exchange(name = "message.center.direct")
    ))
    @RabbitHandler
    public void onLazyMsg(@Payload String msg, @Headers Map<String, Object> headers, Channel channel) throws IOException {
        //消费者操作
        log.info("延迟消息内容：{}", msg);
        channel.basicAck(Long.valueOf(headers.get(AmqpHeaders.DELIVERY_TAG).toString()), false);
        log.info("处理成功");
    }

}

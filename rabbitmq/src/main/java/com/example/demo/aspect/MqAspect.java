package com.example.demo.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/4/1 13:44
 */
@Aspect
@Component
@Order(1)
@Slf4j
public class MqAspect {
    @Pointcut("execution(public * org.springframework.amqp.rabbit.core.RabbitTemplate.convertAndSend(..))")
    public void mqProduct() {
    }

    @Pointcut("execution(public * com.example.demo.consumer..*.*(..))")
    public void mqConsumer() {
    }


    @Around("mqProduct() || mqConsumer()")
    public void Around(ProceedingJoinPoint joinPoint)  {
        Object args[]=joinPoint.getArgs();
        String methodName=joinPoint.getSignature().getName();
        log.info("aop拦截方法：{}", methodName);
        try {
            //主动处理异常，避免消息重复发送出现死循环
            joinPoint.proceed(args);
        } catch (Throwable throwable) {
            log.error(throwable.getMessage(),throwable);
        }

    }
}

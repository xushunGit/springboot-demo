package com.example.demo.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 订单
 *
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/1/14 21:14
 */
@Data
public class Order implements Serializable {
    private String id;
    private String goodsId;
    private Integer goodsNum;
    private String userId;
    private Date orderTime;
    private String msgId;
}

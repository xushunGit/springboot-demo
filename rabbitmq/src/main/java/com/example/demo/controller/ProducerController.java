package com.example.demo.controller;

import com.example.demo.common.QueueEnum;
import com.example.demo.common.QueueName;
import com.example.demo.model.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 生产者
 *
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/1/15 12:26
 */
@RestController
@Slf4j
public class ProducerController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping("sendMsg/{msg}")
    public String sendMsg(@PathVariable String msg) {
        rabbitTemplate.convertAndSend(QueueName.MSG, msg);
        return "send success";
    }


    @PostMapping("addOrder")
    public String addOrder(@RequestBody Order order) {
        rabbitTemplate.convertAndSend(QueueName.ORDER, QueueName.ORDER + "_routing", order);
        return "send success";
    }

    @GetMapping("sendLazyMsg/{seconds}/{msg}")
    public String sendLazyMsg(@PathVariable String msg, @PathVariable Integer seconds) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        msg += ",写入时间：" + dateFormat.format(new Date());
        rabbitTemplate.convertAndSend(QueueEnum.MESSAGE_TTL_QUEUE.getExchange(), QueueEnum.MESSAGE_TTL_QUEUE.getRouteKey(), msg, message -> {
            message.getMessageProperties().setExpiration(String.valueOf(seconds * 1000));
            return message;
        });
        return "send success";
    }

}

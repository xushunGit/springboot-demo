package com.example.demo.init;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/3/30 15:08
 */
@Component
@Slf4j
public class RabbitmqInit implements ApplicationRunner {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Override
    public void run(ApplicationArguments args) {
        rabbitTemplate.setConfirmCallback((@Nullable CorrelationData var1, boolean var2, @Nullable String var3) ->{
            if (var2) {
                log.info("发送成功！");
            } else {
                log.info("发送失败！");
            }
        });
        rabbitTemplate.setMandatory(true);
        //exchange到queue成功,则不回调return
        rabbitTemplate.setReturnCallback((Message var1, int var2, String var3, String var4, String var5) -> {
            log.info("exchange到queue失败，回调信息：{},{},{},{},{}", JSON.toJSONString(var1), var2, var3, var4, var5);
        });
    }
}

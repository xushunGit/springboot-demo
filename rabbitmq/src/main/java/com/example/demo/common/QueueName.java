package com.example.demo.common;

/**
 * 队列名称
 *
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/1/15 12:54
 */
public interface QueueName {
    String MSG = "msg";
    String ORDER = "orders";
}
